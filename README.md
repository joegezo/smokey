# smokey

For solving bandit problems.



We're interested in contextual multi-armed bandit problems where the agent *recommends* choices (by returning a prioritized list), but the actual decisions may be made externally. 

For now, we're assuming rewards for pulling different arms are independent.

Agents will have three methods:

 * `prioritize:` given a set of contexts for each arm, return a prioritized list
 * `choose:` when applicable, combine `prioritize()` with a policy to actually choose a set of arms
 * `update:` given a set of actual choices, contexts, and arms, update the model's parameters

