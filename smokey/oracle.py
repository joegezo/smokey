"""


		oracle.py


Class for running and evaluating regret experiments


"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm

class Oracle(object):
	"""
	Widget for running regret experiments
	"""
	def __init__(self, bandit, agent_dict, num_choices=1):
		"""
		:bandit: data generator
		:agent_dict: dictionary of agents
		:num_choices:
		"""
		self.bandit = bandit
		self.agents = agent_dict
		self.num_choices = num_choices
		self.payouts = {a:[0] for a in agent_dict}
		self.regret = {a:[0] for a in agent_dict}
		self.payouts["max"] = [0]
		self.K = bandit.K
		self.d = bandit.d
		
	def update(self, N=1):
		"""
		Draw a new context vector from the bandit and update
		all the models.
		"""
		for n in tqdm(range(N)):
			contexts = self.bandit.draw_contexts()
			# this is an oracle so we get to see all the rewards
			rewards = self.bandit.get_rewards(np.arange(self.K))
			# what's the best possible reward?
			max_reward = rewards[rewards.argsort()[::-1]][:self.num_choices].sum()
			self.payouts["max"].append(max_reward + self.payouts["max"][-1])
			# now let's run and update each agent
			for a in self.agents:
				priorities = self.agents[a].prioritize(contexts)
				assert len(priorities.shape) == 1, priorities
				choices = priorities[:self.num_choices]
				self.agents[a].update(choices, contexts, rewards[choices])
				self.payouts[a].append(self.agents[a].payout)
				self.regret[a].append(self.payouts["max"][-1] - self.payouts[a][-1])
				
				
	def plot(self):
		"""
		Plot the cumulative regret
		"""
		if "random" in self.agents:
			plt.plot(self.regret["random"], "--", lw=2, label="random")
		 
		for a in self.agents:
			if a != "random":
				plt.plot(self.regret[a], lw=2, label=a)
		plt.xlabel("step")
		plt.ylabel("cumulative regret", fontsize=14)
		plt.legend(loc="upper left", fontsize=14)
		plt.xlim(0, len(self.payouts["max"]))

	def save(self, filename=None):
		"""

		"""
		if filename is None:
			filename = "oracle_output_K=%s_d=%s_choices=%s_T=%s.csv"%(
					self.K, self.d, self.num_choices, len(self.payouts["max"])-1
				)
		df = pd.DataFrame(self.regret)
		df.to_csv(filename)
