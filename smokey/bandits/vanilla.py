"""

			vanilla.py


Boring linear deterministic contextual bandit simulator

"""

import numpy as np



class LinearDeterministicMAB(object):
	"""
	Boring linear deterministic MAB simulator
	"""

	def __init__(self, K, d):
		"""
		:K: number of arms
		:d: dimension of context vectors
		"""
		self.K = K
		self.d = d
		# coefficients for each arm
		self._coefs = np.random.uniform(0, 1, (K,d))
		self._drawn_contexts = False


	def draw_contexts(self):
		"""

		"""
		# generate contexts
		contexts = [np.random.uniform(0, 1, (self.d,)) 
				for _ in range(self.K)]
		# also save rewards so we can query them later
		self._rewards = np.array([self._coefs[k,:].dot(contexts[k])
						for k in range(self.K)])
		self._drawn_contexts = True
		return contexts

	def get_rewards(self, choices):
		"""
		Input an array of indices of chosen arms; return
		the associated rewards
		"""
		assert self._drawn_contexts, "need to draw contexts first"
		self._drawn_contexts = False
		return self._rewards[choices]

	
