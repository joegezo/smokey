"""

			treebootstrap.py


Code for a contextual multi-armed bandit learner based on the 2017 McNellis et
al paper "A Practical Method for Solving Contextual Bandit Problems Using
Decision Trees".

We'll make two small changes to the algorithm:

	-Swapping the decision trees for regression trees, to model arbitrary rewards
	-Limiting the number of context-reward pairs stored for each arm (to allow it
		to adapt to nonstationary processes)

"""

import numpy as np
import pandas as pd
import scipy.stats as st
import multiprocessing as mp

from sklearn.tree import DecisionTreeRegressor
from smokey.agents.vanilla import Agent


def bootstrap_predict(inpt):
	"""
	Input previous results for an arm and a new context
	vector, and return a bootstrapped estimate of the
	reward.

	:inpt: a tuple containing the following:
		:x: stack of context vectors from previous pulls
			of this arm
		:y: rewards associated with previous pulls
		:c: context vector for this round
		:kwargs: any keyword arguments for sklearn's
			DecisionTreeRegressor
	"""
	x, y, c, kwargs = inpt
	N = x.shape[0]

	# compute a bootstrap sample
	bs = np.random.choice(range(N), replace=True, size=N)
	# fit a regression tree to the sample
	model = DecisionTreeRegressor(**kwargs).fit(x[bs,:], y[bs])
	# use the tree to predict the reward for this context vector
	return model.predict(c.reshape(1, -1))





class TreeBootstrapAgent(Agent):
	"""
	Multiprocessing implementation of the TreeBootstrap algorithm
	from "A Practical Method for Solving Contextual Bandit Problems
	Using Decision Trees" by McNellis et al.
	"""

	def __init__(self, K, d, explore=30, maxnum=500, processes=None, **kwargs):
		"""
		:K: number of arms
		:d: dimension of context vectors
		:explore: explore randomly until each arm has been tried at
			least this many times
		:maxnum: max number of vectors to store per arm
		:processes: number of processes to start for pool. If None, 
			uses the number of available cpus.
		"""
		self.K = K
		self.d = d
		self._explore = explore
		self._still_exploring = True
		self._maxnum = maxnum
		self._kwargs = kwargs
		self.payout = 0

		# start mp pool
		if processes is None:
			processes = mp.cpu_count()
		self._pool = mp.Pool(processes = processes)

		# this agent has to store some data- previous results aggregated
		# by arm
		self._dataset = [np.zeros((maxnum,d)) for _ in range(K)]
		self._rewards = [np.zeros(maxnum) for _ in range(K)]

		# also keep track of a "step" for each arm tracking the 
		# number of times it's been called so far
		self._steps = np.zeros(K, dtype=int)


	def predict(self, contexts):
		"""
		Input a list of context vectors and output an array of 
		indices, in descending order of their upper confidence
		bound.
		"""
		# if we're still in the exploration phase, choose a
		# random ordering
		if self._still_exploring:
			return np.random.choice(range(self.K), replace=False, size=self.K)
		# if we're past the exploration phase, make a bootstrapped prediction for
		# each arm's reward
		else:
			inputs = [(self._dataset[i][:self._steps[i],:],
					self._rewards[i][:self._steps[i]],
					contexts[i],
					self._kwargs)
					for i in range(self.K)]

			predicted_rewards = self._pool.map(bootstrap_predict, inputs)

		#return np.argsort(predicted_rewards)[::-1]
		return np.array(predicted_rewards).reshape(-1)


	def update(self, choices, contexts, rewards):
		"""
		:choices: array of indices for C choices
		:contexts: list of C context vectors, each associated with the
			corresponding choice index
		:rewards: array of C rewards
		"""
		# for each arm that was chosen, update the saved parameters
		for i in range(len(choices)):
			c = choices[i]

			# number of times this arm has been called, modulo the number
			# of context-reward pairs we're storing
			ind = int(self._steps[c] % self._maxnum)
			
			# save the context-reward pair and update that arm's step
			self._dataset[c][ind,:] = contexts[c] # i think it's c, not i
			self._rewards[c][ind] = rewards[i]
			self._steps[c] += 1


		# record payout
		self.payout += np.sum(rewards)

		# finally check to see if we should still be exploring
		if self._still_exploring:
			if self._steps.min() >= self._explore:
				self._still_exploring = False


	def __del__(self):
		self._pool.close()














