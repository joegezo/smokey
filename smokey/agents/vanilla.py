"""

				vanilla.py

Generic Agent code

"""
import numpy as np



def top_N_policy(priorities, N=1):
	"""
	Input an array or priorities, return the top N
	"""
	return priorities[:N]


class Agent(object):
	"""
	Prototype class
	"""

	def __init__(self, K, d):
		"""

		"""
		self._policy = top_N_policy
		self.K = K
		self.d = d
		self.payout = 0

	def predict(self, contexts):
		"""

		"""
		return np.random.normal(0, 1, self.K)


	def prioritize(self, contexts):
		"""
		Input a list of context vectors and output an array of
		indices, in descending order of their priority.
		"""
		pred = self.predict(contexts)
		return np.argsort(pred)[::-1]



	def apply_policy(self, ordering, N=1):
		"""
		Apply a decision policy to pick N arms
		"""
		return self._policy(ordering, N)

	def choose(self, contexts, N=1):
		"""
		Given a list of contexts, make a decision
		"""
		priorities = self.prioritize(contexts)
		return self.apply_policy(priorities, N)



	def update(self, choices, contexts, rewards):
		"""

		"""
		self.payout += rewards.sum()


