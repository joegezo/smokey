"""

			linucb.py


Learner for a contextual multi-armed bandit learner, based on the Li et al 2010 paper 
"A Contextual-Bandit Approach to Personalized News Article Recommendation"


Numbered comments correspond to line numbers in Algorithm 1 of the paper.

"""

import numpy as np
from smokey.agents.vanilla import Agent


def compute_p_ta(A, b, x, alpha):
	"""
	Line numbers correspond to Algorithm 1 in the LinUCB paper
	:A: (d,d) array
	:b: (d,1) array
	:x: (d,1) array
	:alpha: float
	"""
	x = x.reshape(-1,1)  # CHECK THIS
	A_inv = np.linalg.inv(A)
	# (8)
	theta = A_inv.dot(b)
	# (9)
	p = theta.T.dot(x) + alpha*np.sqrt(x.T.dot(A_inv).dot(x))
	return p[0,0]




class LinUCBAgent(Agent):
	"""
	Implementation of the disjoint linear upper confidence
	bound agent, as described in Algorithm 1 of "A Contextual-Bandit 
	Approach to Personalized News Article Recommendation" (2010) by
	Li et al.

	We've made one change- an optional decay parameter on the model
	weights, to allow the agent to adapt to nonstationary processes
	more quickly.
	"""
    
	def __init__(self, K, d, alpha=0.5, conc=1., decay=False):
		"""
		:K: number of arms
		:d: dimension of feature space
		:conc: concentration parameter; prior hyperparameter scaling
			of A matrix (not included in Li's paper)
		:decay: Also not in Li's paper- add an optional decay
			rate (in time steps) for the A matrix and b vector.
			This will multiply each A and b by a number (usually
			slightly less than one) during each update step
		"""
		self.K = K
		self.d = d
		self.alpha = alpha
		self.payout = 0
		# initialize parameters for each arm
		self.As = []
		self.bs = []

		for a in range(K):
			self.As.append(conc*np.identity(d))
			self.bs.append(np.zeros(d).reshape((d,1)))
	
		if decay:
			self.decay = 1 - 1/decay
		else:
			self.decay = False
		

	def predict(self, contexts):
		"""
		Input list of context vectors; output
		array of UCBs of forecasted payouts
		"""
		# zip each arm's context vector together
		params = zip(self.As, self.bs, contexts, [self.alpha]*self.K)
		payout_ucbs = np.array(list(map(lambda x: compute_p_ta(*x), params)))
		return payout_ucbs
            
	def _prioritize(self, contexts):
		"""
		Input a list of context vectors and output an array of 
		indices, in descending order of their upper confidence
		bound.
		"""
		# zip each arm's context vector together with its parameters
		#params = zip(self.As, self.bs, contexts, [self.alpha]*self.K)
		# compute the payout UCB for each arm (9)
		#payout_ucbs = np.array(list(map(lambda x: compute_p_ta(*x), params)))
		payout_ucbs = self.predict(contexts)
		# return a prioritized list
		return np.argsort(payout_ucbs)[::-1]



	def update(self, choices, contexts, rewards):
		"""
		Update model parameters based on the payouts of
		a set of choices
        
		:choices: array of indices of the arms chosen this step
		:contexts: list of feature vectors from chosen arms
		:rewards: array or list of payouts for this step
		"""
		for i in range(len(choices)):
			c = choices[i]
			#for ind in choices:
			con = contexts[c].reshape(-1,1)
			# (12)
			self.As[c] += con.dot(con.T)
			# (13)
			self.bs[c] += rewards[i]*con

			# if we're including a decay parameter
			if self.decay:
				self.As[c] *= self.decay
				self.bs[c] *= self.decay

		# keep track of our decay parameter
		self.payout += np.sum(rewards)




